import matplotlib.pyplot as plt

# def apply_acs_layout():
#     style = 'seaborn-v0_8-whitegrid'
#     with plt.style.context(style):
#         plt.axhline(y=0, color='k')
#         plt.axvline(x=0, color='k')
#         plt.grid(True, linestyle='--')
#         plt.xticks(weight = 'bold')
#         plt.yticks(weight = 'bold')

#         # keep all things in the image
#         # plt.tight_layout()
#         plt.subplots_adjust(top=0.85, bottom=0.15, left=0.1)
#         fig = plt.gcf()
#         # golden_ratio = 1.618
#         width = 8
#         # height = width / golden_ratio
#         height = 5
#         fig.set_size_inches(width, height)
#         # fig.set_figwidth(8)
#         # fig.set_figheight(6)
#         fig.set_dpi(100)


def apply_acs_layout(): # dont ask.
    style = 'seaborn-v0_8-whitegrid'
    with plt.style.context(style):
        plt.axhline(y=0, color='k')
        plt.axvline(x=0, color='k')
        plt.grid(True, linestyle='--')
        plt.xticks(weight = 'bold')
        plt.yticks(weight = 'bold')

        # keep all things in the image
        
        plt.subplots_adjust(top=0.75, bottom=0.15, left=0.05, right=0.1)
        plt.tight_layout()
        fig = plt.gcf()
        # golden_ratio = 1.618
        # width = 6
        # height = width / golden_ratio

        width = 9
        height = 4
        # height = 5
        fig.set_size_inches(width, height)
        fig.set_dpi(100)

